/*
 * Copyright 2018-2019 Oliver Berg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing io.getPermissions and
 * limitations under the License.
 */

package moe.kanon.kstructures.util

import java.io.Serializable

/*
   The infix functions can't use "to" like the Pair one does, because of name clashes and how the Pair function works.
   So instead we'll use the name "and", which would normally clash with the infix function for bitwise operations,
    (& literally), but no bitwise operations will be done on these classes.

   Because of that, the syntax for creating a triple & quadruple from the infix functions becomes something akin to;
        10 to 5.0F and 'c' // Triple
        10 to 5.0F and 'c' and false // Quadruple

   The Pair class and it's infix function is able to enclose nigh infinite amount of instances of itself, so one
    might argue that [Tripe] and [Quadruple] serve no real purpose.
   However, while Pair *is* able to do this, a problem/annoyance arises rather quickly when one attempts this.
   Due to the way the Pair class achieves this behaviour, if one wants to actually retrieve the value of a big Tuple
    that was created using the Pair class, a lot of unboxing would need to be done.

   Example;
        val tuple = 0 to 1 to 2 to 3 to 4 to 5 to 6 to 7 to 8 to 9 to 10
    The above code would create a Pair class with one ***very*** deeply nested [Pair] containing everything from 0
     (the first value given) up to 9 (the second-to-last value given.).
    The actual syntax for the class would look like this;
        Pair<Pair<Pair<Pair<Pair<Pair<Pair<Pair<Pair<Pair<Int, Int>, Int>, Int>, Int>, Int>, Int>, Int>, Int>, Int>, Int>
    Which would mean that if you wanted to get the first value given to the Tuple, you'd need to do the following;
        tuple.first.first.first.first.first.first.first.first.first.first
    Needless to say, it's not very pretty.
    Granted, something *as* deeply nested as the tuple showcased here should never actually be done this way, if one
     has that many values that needs to be stored, a List or an Array would be more optimal.
    This is of course also a very extreme case, but I find it to be good to quickly showcase the flaw of the design,
     and using it that way.

    If we wanted to use the Pair "way" to create a Quadruple, it would be something like;
        val quadruple = 0 to 1 to 2 to 3
    Which would generate the following Pair class;
        Pair<Pair<Pair<Int, Int>, Int>, Int>
    This is not *as* bad as the extreme example shown above, but it's still far from optimal, for to access the first
     given value, you'd need to do;
        quad.first.first.first
    Which still doesn't look very good, and would definitely confuse a first-time reader.

    Compare this to how it would look if we used the provided Quadruple class;
        val quadruple = 0 to 1 and 2 and 3
    Which generates the following Quadruple class;
        Quadruple<Int, Int, Int, Int>
    And the syntax for getting the first given value is simply;
        quadruple.first
    Much easier to read, *and* understand what it's actually doing.
*/

/**
 * Creates a [Triple] from the preceding [Pair] and [that].
 */
public infix fun <A, B, C> Pair<A, B>.and(that: C): Triple<A, B, C> = Triple(first, second, that)

/**
 * Creates a [MutableTriple] instance from [this] [(Immutable) Triple][Triple].
 */
public fun <A, B> Pair<A, B>.toMutablePair(): MutablePair<A, B> = MutablePair(this.first, this.second)

/**
 * Creates a [Quadruple] from the preceding [Triple] and [that].
 */
public infix fun <A, B, C, D> Triple<A, B, C>.and(that: D): Quadruple<A, B, C, D> =
    Quadruple(first, second, third, that)

/**
 * Creates a [MutableTriple] instance from [this] [(Immutable) Triple][Triple].
 */
public fun <A, B, C> Triple<A, B, C>.toMutableTriple(): MutableTriple<A, B, C> =
        MutableTriple(this.first, this.second, this.third)

/**
 * Represents a tetrad of values.
 *
 * There is no meaning attached to values in this class, it can be used for any purpose.
 * Quadruple exhibits value semantics, i.e. two quadruples are equal if all four components are equal.
 *
 * @param A type of the first value.
 * @param B type of the second value.
 * @param C type of the third value.
 * @param D type of the fourth value.
 * @property first First value.
 * @property second Second value.
 * @property third Third value.
 * @property fourth Fourth value.
 */
public data class Quadruple<out A, out B, out C, out D>(
    public val first: A,
    public val second: B,
    public val third: C,
    public val fourth: D
) : Serializable {

    /**
     * Returns a [String] representation of the [Quadruple] including its [first], [second], [third] and [fourth] values.
     */
    public override fun toString(): String = "($first, $second, $third, $fourth)"

}

/**
 * Creates a [MutableQuadruple] instance from [this] [(Immutable) Quadruple][Quadruple].
 */
public fun <A, B, C, D> Quadruple<A, B, C, D>.toMutableQuadruple(): MutableQuadruple<A, B, C, D> =
        MutableQuadruple(this.first, this.second, this.third, this.fourth)