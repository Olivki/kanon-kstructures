/*
 * Copyright 2018-2019 Oliver Berg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package moe.kanon.kstructures.util

/**
 * Represents a generic mutable pair of values.
 *
 * There is no meaning attached to values in this class, it can be used for any purpose.
 * Pair exhibits value semantics, i.e. two pairs are equal if both components are equal.
 *
 * @param A type of the first value.
 * @param B type of the second value.
 * @property first First value.
 * @property second Second value.
 * @constructor Creates a new instance of Pair.
 */
public data class MutablePair<A, B>(public var first: A, public var second: B) {
    
    /**
     * Creates an [(Immutable) Pair][Pair] from this [pair][MutablePair].
     */
    public fun toPair(): Pair<A, B> = Pair(first, second)
    
    /**
     * Creates a [MutableTriple] from the preceding [MutablePair] and [that].
     */
    public infix fun <C> and(that: C): MutableTriple<A, B, C> = MutableTriple(first, second, that)
    
    /**
     * Returns string representation of the [Pair] including its [first] and [second] values.
     */
    public override fun toString(): String = "($first, $second)"
    
}

/**
 * Creates a tuple of type [Pair] from this and [that].
 *
 * This can be useful for creating [Map] literals with less noise.
 */
public infix fun <A, B> A.to(that: B): MutablePair<A, B> = MutablePair(this, that)

/**
 * Represents a mutable triad of values
 *
 * There is no meaning attached to values in this class, it can be used for any purpose.
 * Triple exhibits value semantics, i.e. two triples are equal if all three components are equal.
 *
 * @param A type of the first value.
 * @param B type of the second value.
 * @param C type of the third value.
 * @property first First value.
 * @property second Second value.
 * @property third Third value.
 */
public data class MutableTriple<A, B, C>(public var first: A, public var second: B, public var third: C) {
    
    /**
     * Creates an [(Immutable) Triple][Triple] from this [triple][MutableTriple].
     */
    public fun toTriple(): Triple<A, B, C> = Triple(first, second, third)
    
    /**
     * Creates a [MutableQuadruple] from this [MutableTriple] and [that].
     */
    public infix fun <D> and(that: D): MutableQuadruple<A, B, C, D> = MutableQuadruple(first, second, third, that)
    
}

/**
 * Represents a mutable tetrad of values.
 *
 * There is no meaning attached to values in this class, it can be used for any purpose.
 * Quadruple exhibits value semantics, i.e. two quadruples are equal if all four components are equal.
 *
 * @param A type of the first value.
 * @param B type of the second value.
 * @param C type of the third value.
 * @param D type of the fourth value.
 * @property first First value.
 * @property second Second value.
 * @property third Third value.
 * @property fourth Fourth value.
 */
public data class MutableQuadruple<A, B, C, D>(
        public var first: A,
        public var second: B,
        public var third: C,
        public var fourth: D
) {
    
    /**
     * Creates an [(Immutable) Quadruple][Quadruple] from this [quadruple][MutableQuadruple].
     */
    public fun toQuadruple(): Quadruple<A, B, C, D> = Quadruple(first, second, third, fourth)
    
}

