/*
 * Copyright 2019 Oliver Berg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("MemberVisibilityCanBePrivate")

package moe.kanon.kstructures.shapes

import java.awt.Dimension

public data class KDimension(var width: Int, var height: Int) {

    public fun resize(width: Int = this.width, height: Int = this.height) {
        this.width = width
        this.height = height
    }

    public fun translate(width: Int = this.width, height: Int = this.height) {
        this.width += width
        this.height += height
    }

    public fun toJava() = Dimension(width, height)

    // Operators
    public operator fun not() = toJava()

    public operator fun unaryPlus() = KDimension(Math.abs(width), Math.abs(height))

    public operator fun unaryMinus() = KDimension(-width, -height)

    // - Assign operators
    public operator fun plusAssign(other: KDimension) {
        translate(other.width, other.height)
    }

    public operator fun plusAssign(other: Dimension) {
        plusAssign(other.toKotlin())
    }

    public operator fun plusAssign(pair: Pair<Int, Int>) {
        plusAssign(KDimension(pair.first, pair.second))
    }

    public operator fun minusAssign(other: KDimension) {
        translate(-other.width, -other.height)
    }

    public operator fun minusAssign(other: Dimension) {
        minusAssign(other.toKotlin())
    }

    public operator fun minusAssign(pair: Pair<Int, Int>) {
        minusAssign(KDimension(pair.first, pair.second))
    }

    public operator fun timesAssign(other: KDimension) {
        width *= other.width
        height *= other.height
    }

    public operator fun timesAssign(other: Dimension) {
        timesAssign(other.toKotlin())
    }

    public operator fun timesAssign(pair: Pair<Int, Int>) {
        timesAssign(KDimension(pair.first, pair.second))
    }

    public operator fun divAssign(other: KDimension) {
        width /= other.width
        height /= other.height
    }

    public operator fun divAssign(other: Dimension) {
        divAssign(other.toKotlin())
    }

    public operator fun divAssign(pair: Pair<Int, Int>) {
        divAssign(KDimension(pair.first, pair.second))
    }
}

public fun Dimension.toKotlin() = KDimension(width, height)

public operator fun Dimension.not() = toKotlin()