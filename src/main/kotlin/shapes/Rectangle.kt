/*
 * Copyright 2019 Oliver Berg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("MemberVisibilityCanBePrivate")

package moe.kanon.kstructures.shapes

import java.awt.Rectangle

public data class KRectangle(var x: Int, var y: Int, var width: Int, var height: Int) {

    public var location: KPoint
        get() = KPoint(x, y)
        set(location) = resize(x = location.x, y = location.y)

    public var size: KDimension
        get() = KDimension(width, height)
        set(size) = resize(width = size.width, height = size.height)

    public operator fun plus(dimension: KDimension) {
        size += dimension
    }

    public fun resize(x: Int = this.x, y: Int = this.y, width: Int = this.width, height: Int = this.height) {
        if (x == this.x && y == this.y && width == this.width && height == this.height) return

        this.x = x
        this.y = y
        this.width = width
        this.height = height
    }

    public fun translate(x: Int = 0, y: Int = 0, width: Int = 0, height: Int = 0) {
        this.x += x
        this.y += y
        this.width += width
        this.height += height
    }

    public fun toJava(): Rectangle = Rectangle(x, y, width, height)
}

public fun Rectangle.toKotlin(): KRectangle = KRectangle(x, y, width, height)

public operator fun Rectangle.contains(rect: KRectangle): Boolean = intersects(rect.toJava())

public operator fun Rectangle.contains(point: KPoint): Boolean = contains(point.x, point.y)