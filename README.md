# proxus.kstructures

Various data structures for Kotlin.

Some are original creations, while others are ports of Java classes to make them work more nicely with the Kotlin syntax/"way".